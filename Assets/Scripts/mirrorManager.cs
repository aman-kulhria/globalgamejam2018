﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class mirrorManager: MonoBehaviour {

	private RaycastHit _hit;
	private Ray _myRay;
	private Coroutine _coroutine = null;
	private bool _isActive = false;
	private mirrorManager _lastMirrorManager = null;

	public float raycastLength;
	public GameObject light;

	void Start () {
		_coroutine = StartCoroutine (ActiveLight());
	}

	IEnumerator ActiveLight(){

		yield return new WaitForSeconds (2f);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnCollide(Vector3 hitPoint, Vector3 dir, float _angle){

		if (Physics.Raycast (hitPoint, dir, out _hit, raycastLength)) 
		{
			if (_hit.collider.tag == "mirror") {

				Vector3 inDirection = Vector3.Reflect(dir, _hit.normal);

				Debug.DrawRay(_hit.point, _hit.normal*3, Color.blue);  
				//represent the ray using a line that can only be viewed at the scene tab  
				Debug.DrawRay(_hit.point, inDirection*100, Color.magenta);


				float distance = Vector3.Distance (_hit.point, inDirection);

				Vector3 targetDir = _hit.point - light.transform.position;
				float angle = Vector3.Angle(targetDir, inDirection);

				Vector2 diff = _hit.point - light.transform.position;
				float dot = Vector3.Dot (_hit.point, light.transform.position);
				dot = dot / (_hit.point.magnitude * light.transform.position.magnitude);
				float a = Mathf.Acos (dot);
				float aa = a * 180 / Mathf.PI;

				Debug.Log ( "ANGLE  " + aa);

				light.transform.up = - inDirection;
				light.transform.DOScaleX (distance + 1, 0.2f);

				mirrorManager _mirrorManager = _hit.transform.GetComponent<mirrorManager> ();
				_mirrorManager.OnCollide (_hit.point, inDirection, angle);

				return;
			}
		} else {

			if (_lastMirrorManager != null) {
				_lastMirrorManager.ResetLight ();
			}
				
			//light.transform.DOScaleX (raycastLength, 0.1f);
		}

	}


	public void ResetLight(){
		light.transform.DOScaleX (0, 0.1f);
	}

}
