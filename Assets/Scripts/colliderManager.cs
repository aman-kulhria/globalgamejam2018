﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class colliderManager : MonoBehaviour {

	public pipeHandler pipeHandler;
	public Transform nextPoint;
	public Transform _tempPoint = null;
	public Transform _oppositeJoint = null;
	public bool deactive = false;

	private bool isConnected = false;

	void OnTriggerEnter(Collider col)
	{

		if (deactive) {
			return;
		}

		if (col.gameObject.tag == "joint")
		{
			_tempPoint = col.gameObject.GetComponent<colliderManager> ().nextPoint;
			_oppositeJoint = col.gameObject.transform;
			isConnected = true;
			return;
		}

		if (col.gameObject.tag == "ball" && !isConnected) {

			if (nextPoint.GetComponent<colliderManager> ().isConnected) 
			{
				nextPoint.GetComponent<colliderManager> ()._oppositeJoint.GetComponent<colliderManager> ().deactive = true;
				nextPoint.GetComponent<colliderManager> ().deactive = false;
			}

			col.gameObject.transform.DOMove (nextPoint.position, 1f).SetEase(Ease.Linear);
		}

		if (col.gameObject.tag == "ball" && isConnected) {
			nextPoint.GetComponent<colliderManager> ().deactive = false;
			if(nextPoint.GetComponent<colliderManager> ()._oppositeJoint != null)
			nextPoint.GetComponent<colliderManager> ()._oppositeJoint.GetComponent<colliderManager> ().deactive = true;
			col.gameObject.transform.DOMove (_tempPoint.position, 1f).SetEase(Ease.Linear);
		}

		Debug.Log (isConnected);
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "joint") 
		{
			col.gameObject.GetComponent<colliderManager> ().isConnected = false;
			col.gameObject.GetComponent<colliderManager> ().deactive = false;
		}
	}

}
