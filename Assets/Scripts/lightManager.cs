﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class lightManager : MonoBehaviour {

	private RaycastHit _hit;
	private Ray _myRay;
	private bool _isActive = false;
	private mirrorManager _lastMirrorManager = null;

	public float raycastLength;
	public GameObject light;

	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

		_myRay = new Ray (transform.position, transform.right);
		Debug.DrawRay (transform.position, transform.right * 20, Color.green);

		if (Physics.Raycast (_myRay, out _hit, raycastLength))
		{
			if (_hit.collider.tag == "mirror") {

				Vector3 inDirection = Vector3.Reflect(_myRay.direction, _hit.normal);

				Debug.DrawRay(_hit.point, _hit.normal*3, Color.blue);  
				//represent the ray using a line that can only be viewed at the scene tab  
				Debug.DrawRay(_hit.point, inDirection*100, Color.magenta);

				Vector3 targetDir = _hit.point - light.transform.position;
				float angle = Vector3.Angle(targetDir, inDirection);

				Debug.Log (angle + " " + (transform.eulerAngles.z));

				mirrorManager _mirrorManager = _hit.transform.GetComponent<mirrorManager> ();
				_mirrorManager.OnCollide (_hit.point, inDirection, angle);

				float distance = Vector3.Distance (_hit.point, transform.position);

				//light.transform.DORotate (new Vector3(0, 0, angle - (transform.eulerAngles.z)), 0.2f);
				light.transform.DOScaleX (distance + 1, 0.2f);

				return;
			}
		}

		if (_lastMirrorManager != null) {
			_lastMirrorManager.ResetLight ();
			//_mirrorManager = null;
		}

		//light.transform.DOScaleX (raycastLength, 0.1f);
		
	}

	public void OnCollide(){

	}
}
